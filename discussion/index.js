const http = require('http');

const host = 4000;

let server = http.createServer( (req, res) => {
	res.end('Welcome to the App');
})


server.listen(host);

console.log(`Listening on port ${host}`);